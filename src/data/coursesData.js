const coursesData = [
	
	{
		id: "wdc001",
		name: "PHP Laravel",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, doloremque ad consequatur. Rerum, distinctio. Quisquam eos inventore, dolorem accusamus nemo dolorum reprehenderit quo explicabo earum porro, magnam dolores mollitia ipsa.",
		price: 45000,
		onOffer: true
	},

	{
		id: "wdc002",
		name: "Python Django",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, doloremque ad consequatur. Rerum, distinctio. Quisquam eos inventore, dolorem accusamus nemo dolorum reprehenderit quo explicabo earum porro, magnam dolores mollitia ipsa.",
		price: 55000,
		onOffer: true
	},

	{
		id: "wdc003",
		name: "Javascript - Springboot",
		description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quasi, doloremque ad consequatur. Rerum, distinctio. Quisquam eos inventore, dolorem accusamus nemo dolorum reprehenderit quo explicabo earum porro, magnam dolores mollitia ipsa.",
		price: 60000,
		onOffer: true
	}



]


export default coursesData;